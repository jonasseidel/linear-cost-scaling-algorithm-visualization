Purpose
===
Generate latex slides visualizing the operations of the algorithm described in **[1]** on arbitrary graphs. This is the linear variant. For the analogous algorithm operating on graphs with convex costs see [here](https://git.rwth-aachen.de/goseminarss2020/convex-cost-scaling-algorithm-visualization) (a fork of this project).

Use
===
populate Graph like already done in main, execute. Output will be written to ./latex/;  folder created in Graph::Graph. Tested on Pop!\_OS 20.04 LTS; your milage may vary.


---
*  **[1]** Andrew V. Goldberg, Robert E. Tarjan _Solving Minimum-Cost Flow Problems by Successive Approximation_ Proceedings of the nineteenth annual ACM symposium on Theory of computing (1987)
