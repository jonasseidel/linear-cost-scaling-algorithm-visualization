#include "Graph.h"
#include "alg.cpp"
#include <climits>

int main(){


  Graph g = Graph(std::vector<Node*>(), std::vector<Edge*>());
  std::vector<Node*>& nodes = g.get_nodes();
  std::vector<Edge*>& edges = g.get_edges();

  {// populate Graph
    nodes.push_back(new Node(0,0));
    nodes.push_back(new Node(2,-1.5));
    nodes.push_back(new Node(2,1.5));
    nodes.push_back(new Node(4,0));

    edges.push_back(new Edge(nodes[0],nodes[1],7,0,5,-3.5));
    edges.push_back(new Edge(nodes[0],nodes[2],3,0,2,3.5));
    edges.push_back(new Edge(nodes[1],nodes[2],1,0,1,3.5));
    edges.push_back(new Edge(nodes[1],nodes[3],1,0,2,-3.5));
    edges.push_back(new Edge(nodes[2],nodes[3],2,0,1,3.5));
    // target to source
    edges.push_back(new Edge(nodes[3],nodes[0],-7,3,4,-3.5,{{4,2.5},{-.5,2.5},{-.5,1.5/4}}));
  }

#ifndef SUPPRESS_PROTOCOL
  std::cout << g << std::endl;
#endif

  cost_scaling(g);

#ifndef SUPPRESS_PROTOCOL
  std::cout << g << std::endl;
#endif
}
