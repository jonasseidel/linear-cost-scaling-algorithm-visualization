#include "Graph.h"


void refine(Graph& g, double eps){
  std::set<Edge*> changed_edges;
  for(Edge* e : g.get_edges()){
    if(e->reset()){
      changed_edges.insert(e);
    }
  }
  g.generate_slide({{},changed_edges},eps);

  while(true){
    bool terminate = true;
    for(Node* n : g.get_nodes()){ // execute pushes
    repeat_iteration:
      if(n->get_balance() > 0){ // node is active
        terminate = false;

        double min_res_cost = INT_MAX;


        for(Edge* e : n->get_adj()){
          double curr_res_cost = e->get_residual_cost(n);
          double curr_res_cap = e->get_residual_capacity(n);


          if(curr_res_cap > 0){ // pushing possible
            if(min_res_cost > curr_res_cost){ // updating minimum
              min_res_cost = curr_res_cost;
            }

            if(curr_res_cost < 0){ // and pushing improves
              if(n->get_balance() <= curr_res_cap){
                e->set_flow(n,e->get_flow(n)+n->get_balance()); // non-sat push
                g.generate_slide({{}, {e}},eps);
                goto repeat_iteration; // no longer active, so the check will fail and the next iteration begins
              }else{
                e->set_flow(n,e->get_flow(n)+curr_res_cap); // saturating push
              }
              g.generate_slide({{}, {e}},eps);
            }
          }
        }

        double new_potential = min_res_cost+n->get_potential()+eps;
        if(n->get_potential() != new_potential){
          n->set_potential(new_potential
#ifdef CHECK_ASSERT
            , eps
#endif
          );
#ifdef CHECK_ASSERT
          g.check_all_epsilon_optimality();
#endif
          g.generate_slide({{n},{}},eps);
        }

        goto repeat_iteration;
      }
    }

    if(terminate){
#ifndef SUPPRESS_PROTOCOL
      std::cout << g << "\n" << std::endl;
#endif
      break;
    }
    //std::cin.ignore();
  }
}

void cost_scaling(Graph& g){
  g.update_tikzpicture({{},{}});

  double terminate = static_cast<double>(1)/g.get_nodes().size();

  double eps;
  {
    int max = INT_MIN;
    for(Edge* e : g.get_edges()){
      int tmp = std::abs(e->get_cost(e->get_a()));
      if(tmp > max){
        max = tmp;
      }
    }
    eps = max;
  }

  while(eps > terminate){
    eps = eps/2;
    refine(g,eps);
  }
}
