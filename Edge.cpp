#include "Edge.h"

//get state:
Node* Edge::get_a(){
  return a;
}
Node* Edge::get_b(){
  return b;
}
Node* Edge::get_tikz_origin(){
#ifdef DRAW_IMPROVING_EDGES
  return (get_residual_cost(get_a()) < 0 ?  get_a() : get_b());
#else
  return get_a();
#endif
}

double Edge::get_cost(Node* from){
  if(a == from){
    return cost;
  }else{
    return -cost;
  }
}
double Edge::get_min(Node* from){
  if(a == from){
    return min;
  }else{
    return -max;
  }
}
double Edge::get_max(Node* from){
  if(a == from){
    return max;
  }else{
    return -min;
  }
}
double Edge::get_flow(Node* from){
  if(a == from){
    return flow;
  }else{
    return -flow;
  }
}

// derived metrics:
double Edge::get_residual_capacity(Node* from){
  return get_max(from)-get_flow(from);
}

double Edge::get_residual_cost(Node* from){
  Node* to = (a == from) ? b : a;

  return get_cost(from) - from->get_potential() + to->get_potential();
}

// set state:
void Edge::set_a(Node* a){
  this->a = a;
}
void Edge::set_b(Node* b){
  this->b = b;
}
void Edge::set_cost(Node* from, double cost){
  if(a == from){
    this->cost = cost;
  }else{
    this->cost = -cost;
  }
}
void Edge::set_min(Node* from, double min){
  if(a == from){
    this->min = min;
  }else{
    this->max = -min;
  }
}
void Edge::set_max(Node* from, double max){
  if(a == from){
    this->max = max;
  }else{
    this->min = -max;
  }
}
void Edge::set_flow(Node* from, double flow){
  double delta = flow - get_flow(from);

  Node* to = (a == from) ? b : a;
  from->set_balance_delta(delta);
  to->set_balance_delta(-delta);

  if(a == from){
    this->flow = flow;
  }else{
    this->flow = -flow;
  }

#ifndef SUPPRESS_PROTOCOL
  std::cout << "Sending " << delta << " flow down \n " << *this << "\n resulting in : " << flow << "\n form " << *from << std::endl;
#endif
}

// refine reset
bool Edge::reset(){
  if(get_residual_cost(a) < 0){
    set_flow(a,get_max(a));
    return true;
  }else if (get_residual_cost(a) > 0){
    set_flow(a,get_min(a));
    return true;
  }
  return false;
}


Edge::Edge(Node* a, Node* b, double cost, double min, double max, double shift_scalar, std::vector<Node::Pos> line_path) : shift_scalar(shift_scalar), line_path(line_path){
  set_a(a);
  set_b(b);
  set_cost(a,cost);
  set_min(a,min);
  set_max(a,max);
  set_flow(a,min);

  a->add_adj(this);
  b->add_adj(this);
}


std::ostream& operator<<(std::ostream& os, Edge& e){
  os << "Edge " << &e << "{\n";

  os << "\t from Node : " << e.get_a() << "\n";
  os << "\t to Node : " << e.get_b() << "\n";
  os << "\t cost : " << e.get_cost(e.get_a()) << "\n";
  os << "\t min : " << e.get_min(e.get_a()) << "\n";
  os << "\t max : " << e.get_max(e.get_a()) << "\n";
  os << "\t flow : " << e.get_flow(e.get_a()) << "\n";
  os << "\t residuum from-to : " << e.get_residual_capacity(e.get_a()) << "\n";
  os << "\t residuum to-from : " << e.get_residual_capacity(e.get_b()) << "\n";
  os << "\t c_p from-to : " << e.get_residual_cost(e.get_a()) << "\n";
  os << "\t c_p to-from : " << e.get_residual_cost(e.get_b()) << "\n";

  os << "}";

  return os;
}
