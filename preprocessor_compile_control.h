#ifndef SUPPRESS_PROTOCOL
/*
 * delete following line if a protocol should be generated
*/
#define SUPPRESS_PROTOCOL

#endif





#ifndef CHECK_ASSERT
/*
  delete following line if basic assertions should not be checked (for testing)
  leads to asymptotically worse runtime!
*/
//#define CHECK_ASSERT

#endif





#ifndef PEDANTIC_LABELING
/*
  delete if cost and residual capacity should not be given on edges of tikzpictures
*/
//#define PEDANTIC_LABELING

#endif





#ifndef DRAW_IMPROVING_EDGES
/*
  delete if tikz edges should always be drawn from get_a() to get_b()
  Otherwise it will be drawn s.t. the edge has non positive get_residual_cost
  Influence limited to get_tikz_origin()'s return value
*/
#define DRAW_IMPROVING_EDGES

#endif
