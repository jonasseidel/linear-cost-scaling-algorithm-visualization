#ifndef EDGE_H
#define EDGE_H


#include "preprocessor_compile_control.h"

#include <vector>
#include <string>
#include <iostream>
#include <set>

#include "Node.h"


class Edge{
private:
  Node* a;
  Node* b;

  double cost;
  double min;
  double max;
  double flow;

public:
  double shift_scalar;
  std::vector<Node::Pos> line_path;
  //get state:

  Node* get_a();
  Node* get_b();
  Node* get_tikz_origin();
  double get_cost(Node*);
  double get_min(Node*);
  double get_max(Node*);
  double get_flow(Node*);

  // derived metrics:
  double get_residual_capacity(Node*);
  double get_residual_cost(Node*);

  // set state:
  void set_a(Node*);
  void set_b(Node*);
  void set_cost(Node*, double);
  void set_min(Node*, double);
  void set_max(Node*, double);
  void set_flow(Node*, double);

  // refine reset
  bool reset();

  Edge(Node*, Node*, double, double, double, double, std::vector<Node::Pos> = {});
};


std::ostream& operator<<(std::ostream&, Edge&);

#endif
